﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;

    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}



public class ClassPergunta : Quiz
{ // INICIO CLASS PERGUNTAS

    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as RESPOSTAS
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // CHECAR RESPOSTA CORRETA
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }





} // FIM CLASS PERGUNTAS


// CLASS LOGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ //  INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Text PontosTela;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;

    void Start()
    { // INICIO METODO START
      // string texto = pergunta.text;
      // Debug.Log(texto);


        ClassPergunta p1 = new ClassPergunta("Filme onde o personagem principal é um repórter fotográfico que trabalha para um dos dos Jornais mais importantes de sua cidade.", 100);
        p1.AddResposta("Superman", false);
        p1.AddResposta("Doctor Who", false);
        p1.AddResposta("Homem Aranha", true);
        p1.AddResposta("Batman", false);

        ClassPergunta p2 = new ClassPergunta("Com grandes poderes vêm grandes responsabilidades, de que personagem é esta frase ? ", 250);
        p2.AddResposta("Tio Ben", true);
        p2.AddResposta("Goku", false);
        p2.AddResposta("Superman", false);
        p2.AddResposta("Peter Parker", false);

        ClassPergunta p3 = new ClassPergunta("Quem é o “vingador mais forte”, segundo o filme Thor Ragnarok ?", 150);
        p3.AddResposta("Thor", false);
        p3.AddResposta("Hulk", true);
        p3.AddResposta("Fanático", false);
        p3.AddResposta("Homem de Ferro", false);

        ClassPergunta p4 = new ClassPergunta("Qual frase é do filme Exterminador do Futuro 2", 123);
        p4.AddResposta("“Hasta la vista, Baby”", true);
        p4.AddResposta("“Luke, eu sou seu pai”", false);
        p4.AddResposta("“Eu vejo pessoas mortas”", false);
        p4.AddResposta("“E fique com o troco seu animal”", false);

        ClassPergunta p5 = new ClassPergunta("Em Star Wars, qual o planeta natal de Luke Skywalker?", 346);
        p5.AddResposta("Coruscant", false);
        p5.AddResposta("Jakku", false);
        p5.AddResposta("Naboo", false);
        p5.AddResposta("Tatooine", true);

        ClassPergunta p6 = new ClassPergunta("Quem dirigiu o filme Avatar?", 500);
        p6.AddResposta("Steven Spielberg", false);
        p6.AddResposta("James Cameron", true);
        p6.AddResposta("Jim Carey", false);
        p6.AddResposta("Tim Burtton", false);

        ClassPergunta p7 = new ClassPergunta("Qual é o nome completo de Harry Potter?", 453);
        p7.AddResposta("Harry Sirius Potter", false);
        p7.AddResposta("Harry Granger Potter", false);
        p7.AddResposta("Harry Thiago Potter", false);
        p7.AddResposta("Harry James Potter", true);

        ClassPergunta p8 = new ClassPergunta("A frase “My precious...” é de que filme ?", 612);
        p8.AddResposta("O Mágico de OZ", false);
        p8.AddResposta("O Senhor dos Anéis", true);
        p8.AddResposta("Game of Thrones", false);
        p8.AddResposta("O Hobbit", false);

        ClassPergunta p9 = new ClassPergunta("Quem criou os robôs sentinelas em X-men?", 1000);
        p9.AddResposta("William Stryker", false);
        p9.AddResposta("Bolivar Trask", true);
        p9.AddResposta("Magneto", false);
        p9.AddResposta("Mística", false);

        ClassPergunta p10 = new ClassPergunta("O que o personagem Cinna é de Katniss, na saga jogos vorazes?", 1564);
        p10.AddResposta("Produtor", false);
        p10.AddResposta("Empresario", false);
        p10.AddResposta("Inimigo", false);
        p10.AddResposta("Estilista", true);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuizz();


    } // FIM METODO START


    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 2)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();

            }
        }
    }




    private void ExibirPerguntasNoQuizz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }


    public void ProximaPergunta()
    {
        contagemPerguntas++;

        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuizz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PontosTela.text =  pontos.ToString();

        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }


    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            // Debug.Log("Resposta Certa!!!");
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
        }
        else
        {
            // Debug.Log("Resposta errada !!!");
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
        
    }


} // FIM CLASS LOGICA
