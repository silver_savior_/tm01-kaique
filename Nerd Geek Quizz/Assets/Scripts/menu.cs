﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class menu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quizz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Niveis()
    {
        SceneManager.LoadScene("Niveis");
    }

    public void Tutorial()
    {
        SceneManager.LoadScene("Tutorial");
    }

    public void Creditos()
    {
        SceneManager.LoadScene("Creditos");
    }

    public void SairDoJogo()
    {
        Application.Quit();
    }
}

